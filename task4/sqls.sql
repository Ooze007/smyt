﻿SELECT
	p.phone,
	COUNT(*) sold
FROM
	phones p
		INNER JOIN items i ON i.user_id = ANY (p.users)
WHERE
	p.phone IN ('89271398765')
	AND i.status = 7	
GROUP BY
	p.phone;

SELECT 
	p.phone,
	SUM(CASE
		WHEN i.status = 7 THEN 1
		ELSE 0
	END) sold,
	SUM(CASE
		WHEN i.status = 3 THEN 1
		ELSE 0
	END) not_sold
FROM
	phones p
		INNER JOIN items i ON i.user_id = ANY (p.users)
WHERE
	p.phone IN ('89271398765')
	AND i.status IN (3, 7)
GROUP BY
	p.phone;