--
-- PostgreSQL database dump
--

-- Dumped from database version 9.3.6
-- Dumped by pg_dump version 9.3.1
-- Started on 2016-12-10 14:26:47

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 175 (class 3079 OID 11750)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 1964 (class 0 OID 0)
-- Dependencies: 175
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 174 (class 1259 OID 24611)
-- Name: items; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE items (
    id integer NOT NULL,
    user_id integer NOT NULL,
    status smallint NOT NULL,
    CONSTRAINT check_status CHECK ((status = ANY (ARRAY[3, 5, 7])))
);


ALTER TABLE public.items OWNER TO postgres;

--
-- TOC entry 172 (class 1259 OID 24601)
-- Name: phones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE phones (
    phone character varying(11) NOT NULL,
    users integer[]
);


ALTER TABLE public.phones OWNER TO postgres;

--
-- TOC entry 173 (class 1259 OID 24609)
-- Name: tems_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tems_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tems_id_seq OWNER TO postgres;

--
-- TOC entry 1965 (class 0 OID 0)
-- Dependencies: 173
-- Name: tems_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tems_id_seq OWNED BY items.id;


--
-- TOC entry 171 (class 1259 OID 24593)
-- Name: test; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE test (
    "ID" integer NOT NULL,
    "Name" character(255) NOT NULL,
    "Status" boolean DEFAULT true NOT NULL
);


ALTER TABLE public.test OWNER TO postgres;

--
-- TOC entry 170 (class 1259 OID 24591)
-- Name: test_ID_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE "test_ID_seq"
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public."test_ID_seq" OWNER TO postgres;

--
-- TOC entry 1966 (class 0 OID 0)
-- Dependencies: 170
-- Name: test_ID_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE "test_ID_seq" OWNED BY test."ID";


--
-- TOC entry 1836 (class 2604 OID 24614)
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY items ALTER COLUMN id SET DEFAULT nextval('tems_id_seq'::regclass);


--
-- TOC entry 1834 (class 2604 OID 24596)
-- Name: ID; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY test ALTER COLUMN "ID" SET DEFAULT nextval('"test_ID_seq"'::regclass);


--
-- TOC entry 1956 (class 0 OID 24611)
-- Dependencies: 174
-- Data for Name: items; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY items (id, user_id, status) FROM stdin;
1	1	3
2	2	7
3	2	3
7	1	5
\.


--
-- TOC entry 1954 (class 0 OID 24601)
-- Dependencies: 172
-- Data for Name: phones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY phones (phone, users) FROM stdin;
89271398765	{1,2}
\.


--
-- TOC entry 1967 (class 0 OID 0)
-- Dependencies: 173
-- Name: tems_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tems_id_seq', 7, true);


--
-- TOC entry 1953 (class 0 OID 24593)
-- Dependencies: 171
-- Data for Name: test; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY test ("ID", "Name", "Status") FROM stdin;
1	123                                                                                                                                                                                                                                                            	t
2	321                                                                                                                                                                                                                                                            	f
\.


--
-- TOC entry 1968 (class 0 OID 0)
-- Dependencies: 170
-- Name: test_ID_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('"test_ID_seq"', 2, true);


--
-- TOC entry 1839 (class 2606 OID 24599)
-- Name: PRIMARY KEY; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY test
    ADD CONSTRAINT "PRIMARY KEY" PRIMARY KEY ("ID");


--
-- TOC entry 1842 (class 2606 OID 24608)
-- Name: phone_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY phones
    ADD CONSTRAINT phone_pk PRIMARY KEY (phone);


--
-- TOC entry 1844 (class 2606 OID 24616)
-- Name: tems_pk; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY items
    ADD CONSTRAINT tems_pk PRIMARY KEY (id);


--
-- TOC entry 1840 (class 1259 OID 24600)
-- Name: Status; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX "Status" ON test USING btree ("Status");


--
-- TOC entry 1963 (class 0 OID 0)
-- Dependencies: 5
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-12-10 14:27:12

--
-- PostgreSQL database dump complete
--

