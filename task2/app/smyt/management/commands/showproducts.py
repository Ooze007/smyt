"""Show products command module."""
from __future__ import unicode_literals
from django.core.management.base import BaseCommand
from smyt.models import Product


class Command(BaseCommand):
    """Show products command."""

    def handle(self, *args, **options):
        """Command handler."""
        list = Product.objects.select_related(
            'category'
        ).values(
            'category__name', 'name', 'price'
        )

        for item in list:
            self.stdout.write(u'Category: %s' % item['category__name'])
            self.stdout.write(u'Product: %s' % item['name'])
            self.stdout.write(u'Price: %s' % item['price'])
            self.stdout.write('')
