"""Smyt app views."""
from django.shortcuts import render
from models import Category
from django.db.models import Count


# Create your views here.
def home(request):
    """Homepage view."""
    list1 = Category.objects.filter(
        product__price__gte=100
    ).annotate(
        Count('product')
    )

    list2 = Category.objects.filter(
        product__price__gte=100
    ).annotate(
        Count('product')
    ).filter(product__count__gt=10)

    return render(
        request, 'home/index.html',
        {
            'result': list1,
            'result1': list2
        }
    )
