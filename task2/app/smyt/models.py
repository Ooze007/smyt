"""Smyt app models."""
from __future__ import unicode_literals

from django.db import models


# Create your models here.
class Category(models.Model):
    """Category model."""

    name = models.CharField('Group', max_length=64)


class Product(models.Model):
    """Product model."""

    category = models.ForeignKey(Category, verbose_name='Group')
    name = models.CharField('Name', max_length=128)
    price = models.DecimalField(
        'Price',
        max_digits=10,
        decimal_places=2
    )
