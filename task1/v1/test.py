import unittest
import os
import json

import tst

class TestStrParser(unittest.TestCase):
    fixtures = json.load(open(os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'fixture.json'
    )))

    parser = tst.StrParser()

    def testrun(self):
        with self.assertRaises(TypeError):
            self.parser.run(9)

        for fixture in self.fixtures:
            self.assertEqual(
                self.parser.run(fixture['src']),
                fixture['result']
            )

class TestRegParser(unittest.TestCase):
    fixtures = json.load(open(os.path.join(
        os.path.dirname(os.path.abspath(__file__)),
        'fixture.json'
    )))

    parser = tst.RegParser()

    def testrun(self):
        with self.assertRaises(TypeError):
            self.parser.run(9)

        for fixture in self.fixtures:
            self.assertEqual(
                self.parser.run(fixture['src']),
                fixture['result']
            )

if __name__ == '__main__':
    unittest.main()