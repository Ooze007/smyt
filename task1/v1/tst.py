﻿# -*- coding: UTF-8 -*-
import re

class RegParser(object):
    def run(self, value):
        if not (isinstance(value, str) or isinstance(value, unicode)):
            raise TypeError(u'Invalid value type')

        return re.sub(r'(\((?![^\)]*\)).*)', u'', value)


class StrParser(object):
    def run(self, value):
        if not (isinstance(value, str) or isinstance(value, unicode)):
            raise TypeError(u'Invalid value type')

        last_closing = None
        stack = []
        Start = False

        for i in self.__get_pos(value):
            is_open, pos = i

            if is_open:
                stack.append(pos)
                Start = True
            else:
                if len(stack) > 0:
                    start_pos = stack.pop()

                if Start:
                    last_closing = pos

        if len(stack) == 0:
            return value

        if (last_closing is None):
            return value[:min(stack)]

        stack = filter(lambda x: x > last_closing, stack)

        if len(stack) == 0:
            return value

        return value[:min(stack)]

    def __get_pos(self, value):
        start = 0
        str_len = len(value)

        while True:
            opos = value.find('(', start)
            cpos = value.find(')', start)

            if opos == -1 and cpos == -1:
                break

            if opos < 0:
                opos = str_len

            if cpos < 0:
                cpos = str_len

            if opos < cpos:
                start = opos + 1
                yield (True, opos)
            else:
                start = cpos + 1
                yield (False, cpos)
