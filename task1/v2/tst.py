﻿# -*- coding: UTF-8 -*-
import re

class RegParser(object):
    def run(self, value):
        if not (isinstance(value, str) or isinstance(value, unicode)):
            raise TypeError(u'Invalid value type')

        match = re.finditer(r'[()]', value)

        stack = []
        last_closing = None

        for item in match:
            if item.group() == '(':
                stack.append(item.start())
            else:
                if len(stack) > 0:
                    start_pos = stack.pop()
                    last_closing = item.start()

        if len(stack) == 0:
            return value

        if (last_closing is None):
            return value[:min(stack)]

        prev_opens = filter(lambda x: x < last_closing, stack)
        next_opens = filter(lambda x: x > last_closing, stack)

        if len(next_opens) == 0:
            return value[:last_closing + 1]

        if len(prev_opens) == 0:
            return value[:min(next_opens)]

        return value[:max([min(next_opens), last_closing + 1])]


class StrParser(object):
    def run(self, value):
        if not (isinstance(value, str) or isinstance(value, unicode)):
            raise TypeError(u'Invalid value type')

        last_closing = None
        stack = []

        for i in self.__get_pos(value):
            is_open, pos = i

            if is_open:
                stack.append(pos)
            else:
                if len(stack) > 0:
                    start_pos = stack.pop()
                    last_closing = pos

        if len(stack) == 0:
            return value

        if (last_closing is None):
            return value[:min(stack)]

        prev_opens = filter(lambda x: x < last_closing, stack)
        next_opens = filter(lambda x: x > last_closing, stack)

        if len(next_opens) == 0:
            return value[:last_closing + 1]

        if len(prev_opens) == 0:
            return value[:min(next_opens)]

        return value[:max([min(next_opens), last_closing + 1])]

    def __get_pos(self, value):
        start = 0
        str_len = len(value)

        while True:
            opos = value.find('(', start)
            cpos = value.find(')', start)

            if opos == -1 and cpos == -1:
                break

            if opos < 0:
                opos = str_len

            if cpos < 0:
                cpos = str_len

            if opos < cpos:
                start = opos + 1
                yield (True, opos)
            else:
                start = cpos + 1
                yield (False, cpos)

##parser = StrParser()
##parser = RegParser()

##print '['+parser.run(u'test')+']'
##print '-----------------------------------------'
##print '['+parser.run(u'test (598) 123')+']'
##print '-----------------------------------------'
##print '['+parser.run(u'test (598) ((123) 5454')+']'
##print '-----------------------------------------'
##print '['+parser.run(u'test  )) (598) ((123) 5454')+']'
##print '-----------------------------------------'
##print '['+parser.run(u'test (598) (123')+']'
##print '['+parser.run(u'test (598) (123')+']'
##print '-----------------------------------------'
##print '['+parser.run(u'test  )) (598))) ((123) 5454 (')+']'

